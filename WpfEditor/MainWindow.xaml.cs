﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfEditor.Commands;
using WpfEditor.Memento;

namespace WpfEditor;
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private static Invoker invoker = new();

    /// <summary>
    /// Паттерн Singleton тут!
    /// </summary>
    internal static Invoker Invoker => invoker;

    public MainWindow()
    {
        InitializeComponent();
    }

    private void Circle_Click(object sender, RoutedEventArgs e)
    {
        invoker.Run(new CircleCommand(canvas));
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    private void Polygon_Click(object sender, RoutedEventArgs e)
    {
        invoker.Run(new PolygonCommand(canvas));
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    private void Square_Click(object sender, RoutedEventArgs e)
    {
        invoker.Run(new SquareCommand(canvas));
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    private void Undo_Click(object sender, RoutedEventArgs e)
    {
        invoker.Undo();
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    private void Redo_Click(object sender, RoutedEventArgs e)
    {
        invoker.Redo();
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    /// <summary>
    /// Формирование состояния
    /// </summary>
    /// <returns></returns>
    DrawingState SaveState()
    {
        var memento = new DrawingState();
        foreach (Shape shape in canvas.Children)
        {
            Type type = shape.GetType();
            var state = new ShapeState()
            {
                TypeName = type.Name, // краткое имя типа фигуры
                X = Canvas.GetLeft(shape),
                Y = Canvas.GetTop(shape),
                Width = shape.Width,
                Height = shape.Height,
                Color = (shape.Fill as SolidColorBrush).Color.ToString()
            };
            // Для полигона дополнительно сохраним список его точек
            if (shape is Polygon polygon)
            {
                state.Points = new();
                foreach (var point in polygon.Points)
                {
                    state.Points.Add(new PointState()
                    {
                        X = point.X,
                        Y = point.Y
                    });
                }
            }
            // Добавление описания фигуры в состояние
            memento.Shapes.Add(state);
        }
        return memento;
    }

    /// <summary>
    /// Восстановление состояния
    /// </summary>
    /// <param name="memento"></param>
    void RestoreState(DrawingState memento)
    {
        canvas.Children.Clear();
        foreach (var item in memento.Shapes)
        {
            switch (item.TypeName)
            {
                case nameof(Rectangle):
                    invoker.Run(new SquareCommand(canvas, item.X, item.Y));
                    break;
                case nameof(Ellipse):
                    invoker.Run(new CircleCommand(canvas, item.X, item.Y));
                    break;
                case nameof(Polygon):
                    var command = new PolygonCommand(canvas, item.X, item.Y);
                    // Для полигона дополнительно восстановим точки
                    if (command.Shape is Polygon polygon && item.Points != null)
                    {
                        foreach (var point in item.Points)
                        {
                            polygon.Points.Add(new Point(point.X, point.Y));
                        }
                    }
                    invoker.Run(command);
                    break;
            }
        }
        invoker.Clear();
    }

    /// <summary>
    /// Очистка всего
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void New_Click(object sender, RoutedEventArgs e)
    {
        canvas.Children.Clear();
        invoker.Clear();
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    private void Load_Click(object sender, RoutedEventArgs e)
    {
        OpenFileDialog dialog = new()
        {
            Filter = "Файлы JSON (*.json)|*.json|Все файлы (*.*)|*.*"
        };
        if (dialog.ShowDialog() == true)
        {

            string json = System.IO.File.ReadAllText(dialog.FileName);
            var memento = System.Text.Json.JsonSerializer.Deserialize<DrawingState>(json);
            RestoreState(memento);
        }
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }

    private void Save_Click(object sender, RoutedEventArgs e)
    {
        var memento = SaveState();
        SaveFileDialog dialog = new()
        {
            Filter = "Файлы JSON (*.json)|*.json|Все файлы (*.*)|*.*"
        };
        if (dialog.ShowDialog() == true)
        {
            string json = System.Text.Json.JsonSerializer.Serialize(memento);
            System.IO.File.WriteAllText(dialog.FileName, json);
        }
        undoButton.IsEnabled = invoker.CanUndo;
        redoButton.IsEnabled = invoker.CanRedo;
    }
}
