﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WpfEditor.Memento;

/// <summary>
/// Описание фигуры для хранителя
/// </summary>
public class ShapeState : PointState
{
    public string TypeName { get; set; }  

    public double Width { get; set; }

    public double Height { get; set; }

    public string Color { get; set; }

    /// <summary>
    /// Список точек для полигона
    /// </summary>
    public List<PointState>? Points { get; set; }
}
