﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfEditor.Memento;

/// <summary>
/// Точка с координатами
/// </summary>
public class PointState
{
    public double X { get; set; }

    public double Y { get; set; }
}
