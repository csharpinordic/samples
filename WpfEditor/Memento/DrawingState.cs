﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfEditor.Memento;

/// <summary>
/// Класс "Хранитель"
/// </summary>
public class DrawingState
{
    /// <summary>
    /// Список фигур
    /// </summary>
    public List<ShapeState> Shapes { get; set; } = new();
}
