﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using WpfEditor.Interfaces;

namespace WpfEditor.Commands;

class CircleCommand : ShapeCommand
{
    internal CircleCommand(Canvas canvas) : base(canvas) { }

    internal CircleCommand(Canvas canvas, double x, double y) : base(canvas, x, y) { }

    public override void Execute()
        => Execute<Ellipse>(Colors.PeachPuff);
}
