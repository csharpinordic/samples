﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using WpfEditor.Interfaces;

namespace WpfEditor.Commands;

class SquareCommand : ShapeCommand
{
    internal SquareCommand(Canvas canvas) : base(canvas) { }

    internal SquareCommand(Canvas canvas, double x, double y) : base(canvas, x, y) { }

    public override void Execute()
         => Execute<Rectangle>(Colors.Navy);    
}
