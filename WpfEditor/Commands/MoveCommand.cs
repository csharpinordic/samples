﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace WpfEditor.Commands;

internal class MoveCommand : BaseCommand
{
    private Shape shape;

    /// <summary>
    /// Исходная точка, откуда была перемещена фигура
    /// </summary>
    private Point origin;

    /// <summary>
    /// Целевая точка, в которую была перемещена фигура
    /// </summary>
    private Point target;

    internal MoveCommand(Canvas canvas, Shape shape, Point origin) : base(canvas)
    {
        this.shape = shape;
        this.origin = origin;
        target = new Point(Canvas.GetLeft(shape), Canvas.GetTop(shape));
    }

    public override void Execute()
    {
        Canvas.SetLeft(shape, target.X);
        Canvas.SetTop(shape, target.Y);
    }

    public override void Undo()
    {
        Canvas.SetLeft(shape, origin.X);
        Canvas.SetTop(shape, origin.Y);
    }
}
