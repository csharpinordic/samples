﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using WpfEditor.Interfaces;

namespace WpfEditor.Commands;

abstract class BaseCommand : Interfaces.ICommand
{
    private static Logger log = LogManager.GetCurrentClassLogger();

    /// <summary>
    /// receiver - получатель команды
    /// </summary>
    protected readonly Canvas canvas;

    /// <summary>
    /// Точка, с которой нажатая мышь начала движение
    /// </summary>
    private Point from;

    /// <summary>
    /// Исходная точка привязки фигуры
    /// </summary>
    private Point origin;

    /// <summary>
    /// Признак того, что нажата кнопка Control
    /// </summary>
    private bool controlPressed = false;

    /// <summary>
    /// Фигура, которую мы создаем командой
    /// </summary>
    private Shape? shape;

    protected internal Shape? Shape
    {
        get => shape;
        protected set
        {
            shape = value;
            if (shape != null)
            {
                shape.KeyUp += Shape_KeyUp;
                shape.KeyDown += Shape_KeyDown;
                shape.MouseDown += Shape_MouseDown;
                shape.MouseMove += Shape_MouseMove;
                shape.MouseUp += Shape_MouseUp;
            }
        }
    }

    protected BaseCommand(Canvas canvas)
    {
        this.canvas = canvas;
    }

    private void Shape_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.LeftCtrl)
        {
            controlPressed = true;
        }
    }
    private void Shape_KeyUp(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.LeftCtrl)
        {
            controlPressed = false;
        }
    }

    private void Shape_MouseDown(object sender, MouseButtonEventArgs e)
    {
        if (e.LeftButton == MouseButtonState.Pressed)
        {
            if (controlPressed)
            {
                // тут можно было бы сделать копирование
            }

            from = e.GetPosition(canvas);
            origin = new Point(Canvas.GetLeft(shape), Canvas.GetTop(shape));
            log.Trace($"MouseDown: ({from.X}, {from.Y})");
        }
    }

    private void Shape_MouseMove(object sender, MouseEventArgs e)
    {
        if (shape != null && e.LeftButton == MouseButtonState.Pressed)
        {
            var to = e.GetPosition(canvas);
            double dx = to.X - from.X;
            double dy = to.Y - from.Y;
            log.Trace($"MouseMove: ({to.X}, {to.Y}) ({dx}, {dy})");
            Canvas.SetLeft(shape, Canvas.GetLeft(shape) + dx);
            Canvas.SetTop(shape, Canvas.GetTop(shape) + dy);
            from = to;
        }
    }

    private void Shape_MouseUp(object sender, MouseButtonEventArgs e)
    {
        MainWindow.Invoker.Run(new MoveCommand(canvas, shape, origin));
    }

    /// <inheritdoc/>
    public abstract void Execute();

    /// <inheritdoc/>
    public abstract void Undo();
}
