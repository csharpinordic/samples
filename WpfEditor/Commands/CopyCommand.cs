﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfEditor.Commands;

class CopyCommand : BaseCommand
{
    public CopyCommand(Canvas canvas) : base(canvas)
    {
    }

    public override void Execute()
    {
    }

    public override void Undo()
    { 
    }
}
