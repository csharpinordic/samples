﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfEditor.Commands;
internal class PolygonCommand : ShapeCommand
{
    internal PolygonCommand(Canvas canvas) : base(canvas) { }

    internal PolygonCommand(Canvas canvas, double x, double y) : base(canvas, x, y) { }

    public override void Execute()
    {
        Execute<Polygon>(Colors.Red);
        if (Shape is Polygon polygon)
        {
            polygon.Points.Add(new System.Windows.Point(0, 0));
            polygon.Points.Add(new System.Windows.Point(polygon.Width, 0));
            polygon.Points.Add(new System.Windows.Point(polygon.Width, polygon.Height));
        }
    }
}
