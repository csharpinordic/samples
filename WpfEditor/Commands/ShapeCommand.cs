﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfEditor.Commands;

/// <summary>
/// Команда, которая добавляет фигуру
/// </summary>
internal abstract class ShapeCommand : BaseCommand
{
    /// <summary>
    /// Абсцисса фигуры
    /// </summary>
    protected double x;

    /// <summary>
    /// Ордината фигуры
    /// </summary>
    protected double y;

    internal ShapeCommand(Canvas canvas) : base(canvas)
    {
        x = canvas.ActualWidth / 2;
        y = canvas.ActualHeight / 2;
    }

    internal ShapeCommand(Canvas canvas, double x, double y) : base(canvas)
    {
        this.x = x;
        this.y = y;
    }

    /// <summary>
    /// Отмена добавления фигуры
    /// </summary>
    public override void Undo()
    {
        canvas.Children.Remove(Shape);
        Shape = null;
    }

    public void Execute<T>(Color color) where T : Shape, new()
    {
        int size = (int)Math.Min(canvas.ActualWidth / 10, canvas.ActualHeight / 10);
        Shape = new T()
        {
            Width = size,
            Height = size,
            Fill = new SolidColorBrush(color)
        };
        canvas.Children.Add(Shape);
        Canvas.SetLeft(Shape, x);
        Canvas.SetTop(Shape, y);
    }
}
