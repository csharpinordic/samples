﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfEditor.Interfaces;

/// <summary>
/// Команда графического редактора
/// </summary>
interface ICommand
{
    /// <summary>
    /// Выполнить команду
    /// </summary>
    void Execute();

    /// <summary>
    /// Отменить команду
    /// </summary>
    void Undo();
}
