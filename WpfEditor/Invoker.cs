﻿using System.Collections.Generic;
using System.Linq;
using WpfEditor.Interfaces;

namespace WpfEditor;

/// <summary>
/// Исполнитель команд
/// </summary>
class Invoker
{
    /// <summary>
    /// Индекс последней выполненной команды, начиная с 0
    /// <para>-1 - если список команд пуст</para>
    /// </summary>
    private int index = -1;

    /// <summary>
    /// Список выполненных команд
    /// </summary>
    private List<ICommand> commands = new();

    /// <summary>
    /// Выполнение новой команды
    /// </summary>
    /// <param name="command"></param>
    public void Run(ICommand command)
    {
        // Удаление всех отменённых ранее команд
        if  (index < commands.Count - 1)
        {
            commands = commands.Take(index + 1).ToList();
        }
        command.Execute();
        commands.Add(command);
        index++;
    }

    /// <summary>
    /// Есть что отменять
    /// </summary>
    public bool CanUndo => index >= 0;

    /// <summary>
    /// Есть что возвращать
    /// </summary>
    public bool CanRedo => index < commands.Count - 1;

    /// <summary>
    /// Отмена последней команды
    /// </summary>
    public void Undo()
    {
        if (index >= 0)
        {
            ICommand command = commands[index];
            command.Undo();
            index--;
        }
    }

    /// <summary>
    /// Отмена отмены команды
    /// </summary>
    public void Redo()
    {
        if (index < commands.Count - 1)
        {
            index++;
            ICommand command = commands[index];
            command.Execute();
        }
    }

    /// <summary>
    /// Очистка истории команд
    /// </summary>
    public void Clear()
    {
        index = -1;
        commands.Clear();
    }
}
