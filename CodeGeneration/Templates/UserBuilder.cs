﻿/***************************************************************/
/* Файл строится автоматически на основе файла Storage\User.cs */
/***************************************************************/
using CodeGeneration.Storage;

namespace CodeGeneration.Builders;

/// <summary>
/// Построитель пользователя (паттерн Fluend Builder)
/// </summary>
public class UserBuilder
{
    /// <summary>
    /// Настраиваемый пользователь
    /// </summary>
    private User user = new();

    /// <summary>
    /// Установка значения свойства Login
    /// </summary>
    /// <param name="Login">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetLogin(string? login)
    {
        user.Login = login;
        return this;
    }

    /// <summary>
    /// Установка значения свойства FirstName
    /// </summary>
    /// <param name="FirstName">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetFirstName(string? firstName)
    {
        user.FirstName = firstName;
        return this;
    }

    /// <summary>
    /// Установка значения свойства LastName
    /// </summary>
    /// <param name="LastName">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetLastName(string? lastName)
    {
        user.LastName = lastName;
        return this;
    }

    /// <summary>
    /// Установка значения свойства MiddleName
    /// </summary>
    /// <param name="MiddleName">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetMiddleName(string? middleName)
    {
        user.MiddleName = middleName;
        return this;
    }

    /// <summary>
    /// Установка значения свойства Position
    /// </summary>
    /// <param name="Position">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetPosition(string? position)
    {
        user.Position = position;
        return this;
    }

    /// <summary>
    /// Установка значения свойства Email
    /// </summary>
    /// <param name="Email">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetEmail(string? email)
    {
        user.Email = email;
        return this;
    }

    /// <summary>
    /// Установка значения свойства MobilePhone
    /// </summary>
    /// <param name="MobilePhone">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetMobilePhone(string? mobilePhone)
    {
        user.MobilePhone = mobilePhone;
        return this;
    }

    /// <summary>
    /// Установка значения свойства DateOfBirth
    /// </summary>
    /// <param name="DateOfBirth">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetDateOfBirth(DateTime? dateOfBirth)
    {
        user.DateOfBirth = dateOfBirth;
        return this;
    }

    /// <summary>
    /// Установка значения свойства IsMarried
    /// </summary>
    /// <param name="IsMarried">Значение свойства</param>
    /// <returns></returns>
    public UserBuilder SetIsMarried(bool isMarried)
    {
        user.IsMarried = isMarried;
        return this;
    }

    /// <summary>
    /// Создание пользователя
    /// </summary>
    /// <returns></returns>
    public User Create()
    {
        return user;
    }

    /// <summary>
    /// Формирование пользователя
    /// </summary>
    /// <param name="builder">Построитель пользователья</param>
    public static implicit operator User(UserBuilder builder)
        => builder.user;
}
 