﻿namespace CodeGeneration.Storage;

public class User : Entity
{
    public string? Login { get; set; }

    public string? FirstName { get; set; }

    public string? LastName { get; set; }

    public string? MiddleName { get; set; }

    public string? Position { get; set; }

    public string? Email { get; set; }

    public string? MobilePhone { get; set; }

    public DateTime? DateOfBirth { get; set; }

    public bool IsMarried { get; set; } 

    // public static UserBuilder Builder => new UserBuilder();
}
