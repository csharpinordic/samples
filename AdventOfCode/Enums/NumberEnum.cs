﻿namespace AdventOfCode.Enums;

public enum NumberEnum
{
    One = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
}
