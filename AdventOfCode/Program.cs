﻿using AdventOfCode.Enums;
using System.Text.RegularExpressions;

namespace AdventOfCode;

/// <summary>
/// https://adventofcode.com/2023
/// </summary>
internal static class Program
{
    private static int ten = 10;

    private static Dictionary<string, int> Digits = new();

    private static string DigitNames;

    private static int Parse(string s)
    {
        if (int.TryParse(s, out int digit))
            return digit;
        return Digits[s];
    }

    private static int Puzzle1()
    {
        string[] lines = System.IO.File.ReadAllLines("day1.txt");
        int summa = 0;
        foreach (string line in lines)
        {
            var match = Regex.Match(line, $@"^\D*?(\d|{DigitNames}).*(\d|{DigitNames})\D*?$");
            if (!match.Success)
            {
                match = Regex.Match(line, $@"^\D*?(\d|{DigitNames})\D*?$");
                if (!match.Success)
                {
                    /// Console.WriteLine(line);
                    continue;
                }
                int digit = Parse(match.Groups[1].Value);
                int number1 = ten * digit + digit;
                summa += number1;
                /// Console.WriteLine($"{line} = {number1}");
                continue;
            }
            int digit1 = Parse(match.Groups[1].Value);
            int digit2 = Parse(match.Groups[2].Value);
            int number2 = ten * digit1 + digit2;
            /// Console.WriteLine($"{line} = {number2}");
            summa += number2;
        }
        return summa;
    }

    static void Main(string[] args)
    {
        foreach (var digit in Enum.GetValues<NumberEnum>())
        { 
            Digits.Add(digit.ToString().ToLower(), (int)digit);
        }
        DigitNames = string.Join('|', Digits.Keys);

        Console.WriteLine($"Day 1 = {Puzzle1()}");
    }
}
